
library IEEE;
use IEEE.numeric_bit.ALL;

entity conv is
    Port ( r_a1 : in  bit_vector (7 downto 0);
           r_a2 : in  bit_vector (7 downto 0);
           r_a3 : in  bit_vector (7 downto 0);
           r_a4 : in  bit_vector (7 downto 0);
           r_a5 : in  bit_vector (7 downto 0);
           r_a6 : in  bit_vector (7 downto 0);
           r_a7 : in  bit_vector (7 downto 0);
           r_a8 : in  bit_vector (7 downto 0);
           r_a9 : in  bit_vector (7 downto 0);
		   
		     g_a1 : in  bit_vector (7 downto 0);
           g_a2 : in  bit_vector (7 downto 0);
           g_a3 : in  bit_vector (7 downto 0);
           g_a4 : in  bit_vector (7 downto 0);
           g_a5 : in  bit_vector (7 downto 0);
           g_a6 : in  bit_vector (7 downto 0);
           g_a7 : in  bit_vector (7 downto 0);
           g_a8 : in  bit_vector (7 downto 0);
           g_a9 : in  bit_vector (7 downto 0);
		   
		     b_a1 : in  bit_vector (7 downto 0);
           b_a2 : in  bit_vector (7 downto 0);
           b_a3 : in  bit_vector (7 downto 0);
           b_a4 : in  bit_vector (7 downto 0);
           b_a5 : in  bit_vector (7 downto 0);
           b_a6 : in  bit_vector (7 downto 0);
           b_a7 : in  bit_vector (7 downto 0);
           b_a8 : in  bit_vector (7 downto 0);
           b_a9 : in  bit_vector (7 downto 0);
           f : out  bit_vector (34 downto 0));
end conv;

architecture Behavioral of conv is


component filter_333
    Port ( x1 : in  bit_vector (7 downto 0);
           x2 : in  bit_vector (7 downto 0);
           x3 : in  bit_vector (7 downto 0);
           x4 : in  bit_vector (7 downto 0);
           x5 : in  bit_vector (7 downto 0);
           x6 : in  bit_vector (7 downto 0);
           x7 : in  bit_vector (7 downto 0);
           x8 : in  bit_vector (7 downto 0);
           x9 : in  bit_vector (7 downto 0);
			  
			  x10 : in  bit_vector (7 downto 0);
			  x11 : in  bit_vector (7 downto 0);
			  x12 : in  bit_vector (7 downto 0);
			  x13 : in  bit_vector (7 downto 0);
			  x14 : in  bit_vector (7 downto 0);
			  x15 : in  bit_vector (7 downto 0);
			  x16 : in  bit_vector (7 downto 0);
			  x17 : in  bit_vector (7 downto 0);
			  x18 : in  bit_vector (7 downto 0);
			  
			  x19 : in  bit_vector (7 downto 0);
			  x20 : in  bit_vector (7 downto 0);
			  x21 : in  bit_vector (7 downto 0);
			  x22 : in  bit_vector (7 downto 0);
			  x23 : in  bit_vector (7 downto 0);
			  x24 : in  bit_vector (7 downto 0);
			  x25 : in  bit_vector (7 downto 0);
			  x26 : in  bit_vector (7 downto 0);
			  x27 : in  bit_vector (7 downto 0);
			  
           y : out  bit_vector (44 downto 0));
end component;

signal rez : bit_vector (44 downto 0);

begin

filter_gauss : filter_333 PORT MAP( x1 => r_a1, x2 => r_a2, x3 => r_a3, x4 => r_a4, x5 => r_a5, 
										  x6 => r_a6, x7 => r_a7, x8 => r_a8, x9 => r_a9, 
										  x10 => g_a1, x11 => g_a2, x12 => g_a3, x13 => g_a4, x14 => g_a5, 
										  x15 => g_a6, x16 => g_a7, x17 => g_a8, x18 => g_a9,
										  x19 => b_a1, x20 => b_a2, x21 => b_a3, x22 => b_a4, x23 => b_a5, 
										  x24 => b_a6, x25 => b_a7, x26 => b_a8, x27 => b_a9,
										  y => rez);														

f <= rez(44 downto 10);

end Behavioral;

