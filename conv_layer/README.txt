Hardware implementation of convolution with filter mask 3x3x3

Programming language is VHDL

The project contains of 5 folders

* filter_two's_comp - This is convilution implementation in two's complement

* filter_RNS - This is a convolution implementation using  calculations by various moduli of type 2^n and 2^n-1

* filter_RNS_example_1, *filter_RNS_example_2, *filter_RNS_example_3 - This is a convolution implementation using Residue Number System with various moduli set

Project requires a library prefix_mod_lib.vhd connection

