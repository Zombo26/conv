library IEEE;
use IEEE.numeric_bit.ALL;
use work.prefix_mod_lib.all;
entity filter_mod_2p3m1 is
   Port (
       x1 : in bit_vector (2 downto 0);
       x2 : in bit_vector (2 downto 0);
       x3 : in bit_vector (2 downto 0);
       x4 : in bit_vector (2 downto 0);
       x5 : in bit_vector (2 downto 0);
       x6 : in bit_vector (2 downto 0);
       x7 : in bit_vector (2 downto 0);
       x8 : in bit_vector (2 downto 0);
       x9 : in bit_vector (2 downto 0);
       x10 : in bit_vector (2 downto 0);
       x11 : in bit_vector (2 downto 0);
       x12 : in bit_vector (2 downto 0);
       x13 : in bit_vector (2 downto 0);
       x14 : in bit_vector (2 downto 0);
       x15 : in bit_vector (2 downto 0);
       x16 : in bit_vector (2 downto 0);
       x17 : in bit_vector (2 downto 0);
       x18 : in bit_vector (2 downto 0);
       x19 : in bit_vector (2 downto 0);
       x20 : in bit_vector (2 downto 0);
       x21 : in bit_vector (2 downto 0);
       x22 : in bit_vector (2 downto 0);
       x23 : in bit_vector (2 downto 0);
       x24 : in bit_vector (2 downto 0);
       x25 : in bit_vector (2 downto 0);
       x26 : in bit_vector (2 downto 0);
       x27 : in bit_vector (2 downto 0);
       y : out bit_vector (2 downto 0));
end filter_mod_2p3m1;
architecture Behavioral of filter_mod_2p3m1 is
constant n : natural :=3;
signal p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23 : bit_vector (n-1 downto 0);
signal sum1, sum2, sum3, sum4, sum5, sum6, sum7, sum8, sum9, sum10, sum11, sum12, sum13, sum14, sum15, sum16, sum17, sum18, sum19, sum20, sum21 : bit_vector (n-1 downto 0);
signal carry1, carry2, carry3, carry4, carry5, carry6, carry7, carry8, carry9, carry10, carry11, carry12, carry13, carry14, carry15, carry16, carry17, carry18, carry19, carry20, carry21 : bit_vector (n-1 downto 0);
begin
p1(0) <= x1(1); p1(1) <= x1(2); p1(2) <= x1(0); 
p2(0) <= x2(1); p2(1) <= x2(2); p2(2) <= x2(0); 
p3(0) <= x5(2); p3(1) <= x5(0); p3(2) <= x5(1); 
p4(0) <= not x6(0); p4(1) <= not x6(1); p4(2) <= not x6(2); 
p5(0) <= not x7(1); p5(1) <= not x7(2); p5(2) <= not x7(0); 
p6(0) <= not x8(1); p6(1) <= not x8(2); p6(2) <= not x8(0); 
p7(0) <= not x9(0); p7(1) <= not x9(1); p7(2) <= not x9(2); 
p8(0) <= not x10(0); p8(1) <= not x10(1); p8(2) <= not x10(2); 
p9(0) <= not x11(1); p9(1) <= not x11(2); p9(2) <= not x11(0); 
p10(0) <= x12(1); p10(1) <= x12(2); p10(2) <= x12(0); 
p11(0) <= not x13(2); p11(1) <= not x13(0); p11(2) <= not x13(1); 
p12(0) <= x14(1); p12(1) <= x14(2); p12(2) <= x14(0); 
p13(0) <= x15(0); p13(1) <= x15(1); p13(2) <= x15(2); 
p14(0) <= x16(2); p14(1) <= x16(0); p14(2) <= x16(1); 
p15(0) <= x18(1); p15(1) <= x18(2); p15(2) <= x18(0); 
p16(0) <= x19(0); p16(1) <= x19(1); p16(2) <= x19(2); 
p17(0) <= x20(1); p17(1) <= x20(2); p17(2) <= x20(0); 
p18(0) <= x21(1); p18(1) <= x21(2); p18(2) <= x21(0); 
p19(0) <= x22(0); p19(1) <= x22(1); p19(2) <= x22(2); 
p20(0) <= x23(2); p20(1) <= x23(0); p20(2) <= x23(1); 
p21(0) <= not x25(2); p21(1) <= not x25(0); p21(2) <= not x25(1); 
p22(0) <= x27(0); p22(1) <= x27(1); p22(2) <= x27(2); 
p23 <= "110";
EAC_CSA (p1, p2, p3, n, sum1, carry1);
EAC_CSA (p4, p5, p6, n, sum2, carry2);
EAC_CSA (p7, p8, p9, n, sum3, carry3);
EAC_CSA (p10, p11, p12, n, sum4, carry4);
EAC_CSA (p13, p14, p15, n, sum5, carry5);
EAC_CSA (p16, p17, p18, n, sum6, carry6);
EAC_CSA (p19, p20, p21, n, sum7, carry7);
EAC_CSA (sum1, carry1, sum2, n, sum8, carry8);
EAC_CSA (carry2, sum3, carry3, n, sum9, carry9);
EAC_CSA (sum4, carry4, sum5, n, sum10, carry10);
EAC_CSA (carry5, sum6, carry6, n, sum11, carry11);
EAC_CSA (sum7, carry7, p22, n, sum12, carry12);
EAC_CSA (sum8, carry8, sum9, n, sum13, carry13);
EAC_CSA (carry9, sum10, carry10, n, sum14, carry14);
EAC_CSA (sum11, carry11, sum12, n, sum15, carry15);
EAC_CSA (sum13, carry13, sum14, n, sum16, carry16);
EAC_CSA (carry14, sum15, carry15, n, sum17, carry17);
EAC_CSA (sum16, carry16, sum17, n, sum18, carry18);
EAC_CSA (sum18, carry18, carry17, n, sum19, carry19);
EAC_CSA (sum19, carry19, carry12, n, sum20, carry20);
EAC_CSA (sum20, carry20, p23, n, sum21, carry21);
y <= EAC_pref_add(sum21, carry21, n);
end Behavioral;
