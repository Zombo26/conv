----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:12:05 08/31/2017 
-- Design Name: 
-- Module Name:    pss_to_rns - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.numeric_bit.ALL;
use work.prefix_mod_lib.all;

entity pss_to_rns is
    Port ( x : in  bit_vector (7 downto 0);
           y1 : out  bit_vector (4 downto 0);
           y2 : out  bit_vector (8 downto 0);
           y3 : out  bit_vector (8 downto 0));
end pss_to_rns;

architecture Behavioral of pss_to_rns is

constant k : natural := 8;
constant n1 : natural := 5;
--constant n2 : natural := 9;
--constant n3 : natural := 7;


begin
y1 <= mod_2pnm1 ( x, n1, k );
y2 (k-1 downto 0) <= x;
y3 (k-1 downto 0) <= x;

end Behavioral;

