library IEEE;
use IEEE.numeric_bit.ALL;
use work.prefix_mod_lib.all;
entity rns_to_pss is
   Port (
       y1 : in bit_vector (4 downto 0);
       y2 : in bit_vector (8 downto 0);
       y3 : in bit_vector (6 downto 0);
       z : out bit_vector (20 downto 0));--------------------------
end rns_to_pss;
architecture Behavioral of rns_to_pss is
constant n : natural :=30;
signal p1, p2, p3, p4, p5 : bit_vector (n-1 downto 0);
signal sum1, sum2, sum3 : bit_vector (n-1 downto 0);
signal carry1, carry2, carry3 : bit_vector (n-1 downto 0);
signal rez : bit_vector (n-1 downto 0);

constant k : natural :=51;
signal p6, p7, p8, p9, p10, p11, p12, p13, p14 : bit_vector (k-1 downto 0);
signal sum4, sum5, sum6, sum7, sum8, sum9, sum10  : bit_vector (k-1 downto 0);
signal carry4, carry5, carry6, carry7, carry8, carry9, carry10 : bit_vector (k-1 downto 0);
signal rez_2 : bit_vector (k-1 downto 0);

begin
p1(0) <= y2(0); p1(1) <= y2(1); p1(2) <= y2(2); p1(3) <= y2(3); p1(4) <= not y1(0); p1(5) <= not y1(1); p1(6) <= not y1(2); p1(7) <= not y1(3); p1(8) <= not y1(4); p1(9) <= not y1(0); p1(10) <= not y1(1); p1(11) <= not y1(2); p1(12) <= not y1(3); p1(13) <= not y1(4); p1(14) <= not y1(0); p1(15) <= not y1(1); p1(16) <= not y1(2); p1(17) <= not y1(3); p1(18) <= not y1(4); p1(19) <= not y1(0); p1(20) <= not y1(1); p1(21) <= not y1(2); p1(22) <= not y1(3); p1(23) <= not y1(4); p1(24) <= not y1(0); p1(25) <= not y1(1); p1(26) <= not y1(2); p1(27) <= not y1(3); p1(28) <= not y1(4); p1(29) <= not y1(0); 
p2(1) <= y2(0); p2(2) <= y2(1); p2(3) <= y2(2); p2(4) <= y2(4); p2(5) <= y2(5); p2(6) <= y2(6); p2(7) <= y2(7); p2(8) <= y2(8); p2(9) <= y2(8); p2(10) <= y2(5); p2(11) <= y2(6); p2(12) <= y2(7); p2(13) <= y2(8); p2(14) <= y2(4); p2(15) <= y2(5); p2(16) <= y2(6); p2(17) <= y2(7); p2(18) <= y2(8); p2(19) <= y2(5); p2(20) <= y2(6); p2(21) <= y2(7); p2(22) <= y2(8); p2(23) <= y2(4); p2(24) <= y2(5); p2(25) <= y2(6); p2(26) <= y2(7); p2(27) <= y2(8); p2(28) <= y2(5); p2(29) <= y2(6); 
p3(4) <= y2(3); p3(5) <= y2(4); p3(6) <= y2(5); p3(7) <= y2(6); p3(8) <= y2(7); p3(9) <= y2(4); p3(10) <= y2(0); p3(11) <= y2(1); p3(12) <= y2(2); p3(13) <= y2(3); p3(14) <= y2(0); p3(15) <= y2(1); p3(16) <= y2(2); p3(17) <= y2(3); p3(18) <= y2(4); p3(19) <= y2(0); p3(20) <= y2(1); p3(21) <= y2(2); p3(22) <= y2(3); p3(23) <= y2(0); p3(24) <= y2(1); p3(25) <= y2(2); p3(26) <= y2(3); p3(27) <= y2(4); p3(28) <= y2(0); p3(29) <= y2(1); 
p4(4) <= '1'; p4(5) <= y2(0); p4(6) <= y2(1); p4(7) <= y2(2); p4(8) <= y2(3); p4(23) <= y3(0); p4(24) <= y3(1); p4(25) <= y3(2); p4(26) <= y3(3); p4(27) <= y3(4); p4(28) <= y3(5); p4(29) <= y3(6); 
p5(28) <= y3(0); p5(29) <= y3(1); 
CSA_mod2n (p1, p2, p3, n, sum1, carry1);
CSA_mod2n (sum1, carry1, p4, n, sum2, carry2);
CSA_mod2n (sum2, carry2, p5, n, sum3, carry3);
rez <= mod_2n_pref_add(sum3, carry3, n);
--------------------------------------------------------------

p6(7) <= rez(0); p6(8) <= rez(1); p6(9) <= rez(2); p6(10) <= rez(3); p6(11) <= rez(4); p6(12) <= rez(5); p6(13) <= rez(6); p6(14) <= rez(7); p6(15) <= rez(8); p6(16) <= rez(9); p6(17) <= rez(10); p6(18) <= rez(11); p6(19) <= rez(12); p6(20) <= rez(13); p6(21) <= rez(14); p6(22) <= rez(15); p6(23) <= rez(16); p6(24) <= rez(17); p6(25) <= rez(18); p6(26) <= rez(19); p6(27) <= rez(20); p6(28) <= rez(21); p6(29) <= rez(22); p6(30) <= rez(23); p6(31) <= rez(24); p6(32) <= rez(25); p6(33) <= rez(26); p6(34) <= rez(27); p6(35) <= rez(28); p6(36) <= rez(29); p6(37) <= rez(25); p6(38) <= rez(26); p6(39) <= rez(27); p6(40) <= rez(28); p6(41) <= rez(29); p6(42) <= rez(29); p6(43) <= rez(29); p6(44) <= rez(29); p6(45) <= rez(28); p6(46) <= rez(29); p6(47) <= rez(29); p6(48) <= rez(29); p6(49) <= rez(29); 
p7(12) <= rez(0); p7(13) <= rez(1); p7(14) <= rez(2); p7(15) <= rez(3); p7(16) <= rez(4); p7(17) <= rez(5); p7(18) <= rez(6); p7(19) <= rez(7); p7(20) <= rez(8); p7(21) <= rez(9); p7(22) <= rez(10); p7(23) <= rez(11); p7(24) <= rez(12); p7(25) <= rez(13); p7(26) <= rez(14); p7(27) <= rez(15); p7(28) <= rez(16); p7(29) <= rez(17); p7(30) <= rez(18); p7(31) <= rez(19); p7(32) <= rez(20); p7(33) <= rez(21); p7(34) <= rez(22); p7(35) <= rez(23); p7(36) <= rez(24); p7(37) <= rez(24); p7(38) <= rez(25); p7(39) <= rez(26); p7(40) <= rez(27); p7(41) <= rez(28); p7(42) <= rez(28); p7(43) <= rez(28); p7(44) <= rez(27); p7(45) <= rez(27); p7(46) <= rez(28); p7(47) <= rez(28); p7(48) <= rez(28); 
p8(13) <= rez(0); p8(14) <= rez(1); p8(15) <= rez(2); p8(16) <= rez(3); p8(17) <= rez(4); p8(18) <= rez(5); p8(19) <= rez(6); p8(20) <= rez(7); p8(21) <= rez(8); p8(22) <= rez(9); p8(23) <= rez(10); p8(24) <= rez(11); p8(25) <= rez(12); p8(26) <= rez(13); p8(27) <= rez(14); p8(28) <= rez(15); p8(29) <= rez(16); p8(30) <= rez(17); p8(31) <= rez(18); p8(32) <= rez(19); p8(33) <= rez(20); p8(34) <= rez(21); p8(35) <= rez(22); p8(36) <= rez(23); p8(37) <= rez(23); p8(38) <= rez(24); p8(39) <= rez(25); p8(40) <= rez(26); p8(41) <= rez(27); p8(42) <= rez(27); p8(43) <= rez(26); p8(44) <= rez(26); p8(45) <= rez(26); p8(46) <= rez(27); p8(47) <= rez(27); 
p9(14) <= rez(0); p9(15) <= rez(1); p9(16) <= rez(2); p9(17) <= rez(3); p9(18) <= rez(4); p9(19) <= rez(5); p9(20) <= rez(6); p9(21) <= rez(7); p9(22) <= rez(8); p9(23) <= rez(9); p9(24) <= rez(10); p9(25) <= rez(11); p9(26) <= rez(12); p9(27) <= rez(13); p9(28) <= rez(14); p9(29) <= rez(15); p9(30) <= rez(16); p9(31) <= rez(17); p9(32) <= rez(18); p9(33) <= rez(19); p9(34) <= rez(20); p9(35) <= rez(21); p9(36) <= rez(22); p9(37) <= rez(22); p9(38) <= rez(23); p9(39) <= rez(24); p9(40) <= rez(25); p9(41) <= rez(26); p9(42) <= rez(25); p9(43) <= rez(25); p9(44) <= rez(25); p9(45) <= rez(25); p9(46) <= rez(26); 
p10(15) <= rez(0); p10(16) <= rez(1); p10(17) <= rez(2); p10(18) <= rez(3); p10(19) <= rez(4); p10(20) <= rez(5); p10(21) <= rez(6); p10(22) <= rez(7); p10(23) <= rez(8); p10(24) <= rez(9); p10(25) <= rez(10); p10(26) <= rez(11); p10(27) <= rez(12); p10(28) <= rez(13); p10(29) <= rez(14); p10(30) <= rez(15); p10(31) <= rez(16); p10(32) <= rez(17); p10(33) <= rez(18); p10(34) <= rez(19); p10(35) <= rez(20); p10(36) <= rez(21); p10(37) <= rez(20); p10(38) <= rez(21); p10(39) <= rez(22); p10(40) <= rez(23); p10(41) <= rez(24); p10(42) <= rez(24); p10(43) <= rez(24); p10(44) <= rez(24); 
p11(17) <= rez(0); p11(18) <= rez(1); p11(19) <= rez(2); p11(20) <= rez(3); p11(21) <= rez(4); p11(22) <= rez(5); p11(23) <= rez(6); p11(24) <= rez(7); p11(25) <= rez(8); p11(26) <= rez(9); p11(27) <= rez(10); p11(28) <= rez(11); p11(29) <= rez(12); p11(30) <= rez(13); p11(31) <= rez(14); p11(32) <= rez(15); p11(33) <= rez(16); p11(34) <= rez(17); p11(35) <= rez(18); p11(36) <= rez(19); p11(37) <= rez(19); p11(38) <= rez(20); p11(39) <= rez(21); p11(40) <= rez(22); p11(41) <= rez(23); p11(42) <= rez(23); p11(43) <= rez(23); 
p12(18) <= rez(0); p12(19) <= rez(1); p12(20) <= rez(2); p12(21) <= rez(3); p12(22) <= rez(4); p12(23) <= rez(5); p12(24) <= rez(6); p12(25) <= rez(7); p12(26) <= rez(8); p12(27) <= rez(9); p12(28) <= rez(10); p12(29) <= rez(11); p12(30) <= rez(12); p12(31) <= rez(13); p12(32) <= rez(14); p12(33) <= rez(15); p12(34) <= rez(16); p12(35) <= rez(17); p12(36) <= rez(18); p12(37) <= rez(18); p12(38) <= rez(19); p12(39) <= rez(20); p12(40) <= rez(21); p12(41) <= rez(22); p12(42) <= rez(22); 
p13(19) <= rez(0); p13(20) <= rez(1); p13(21) <= rez(2); p13(22) <= rez(3); p13(23) <= rez(4); p13(24) <= rez(5); p13(25) <= rez(6); p13(26) <= rez(7); p13(27) <= rez(8); p13(28) <= rez(9); p13(29) <= rez(10); p13(30) <= rez(11); p13(31) <= rez(12); p13(32) <= rez(13); p13(33) <= rez(14); p13(34) <= rez(15); p13(35) <= rez(16); p13(36) <= rez(17); p13(37) <= rez(17); p13(38) <= rez(18); p13(39) <= rez(19); p13(40) <= rez(20); p13(41) <= rez(21); 
p14(20) <= rez(0); p14(21) <= rez(1); p14(22) <= rez(2); p14(23) <= rez(3); p14(24) <= rez(4); p14(25) <= rez(5); p14(26) <= rez(6); p14(27) <= rez(7); p14(28) <= rez(8); p14(29) <= rez(9); p14(30) <= rez(10); p14(31) <= rez(11); p14(32) <= rez(12); p14(33) <= rez(13); p14(34) <= rez(14); p14(35) <= rez(15); p14(36) <= rez(16); 
CSA_mod2n (p6, p7, p8, k, sum4, carry4);
CSA_mod2n (p9, p10, p11, k, sum5, carry5);
CSA_mod2n (p12, p13, p14, k, sum6, carry6);
CSA_mod2n (sum4, carry4, sum5, k, sum7, carry7);
CSA_mod2n (carry5, sum6, carry6, k, sum8, carry8);
CSA_mod2n (sum7, carry7, sum8, k, sum9, carry9);
CSA_mod2n (sum9, carry9, carry8, k, sum10, carry10);
rez_2 <= mod_2n_pref_add(sum10, carry10, k);

z <= rez_2(k-1 downto n);
end Behavioral;
