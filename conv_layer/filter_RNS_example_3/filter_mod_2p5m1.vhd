library IEEE;
use IEEE.numeric_bit.ALL;
use work.prefix_mod_lib.all;
entity filter_mod_2p5m1 is
   Port (
       x1 : in bit_vector (4 downto 0);
       x2 : in bit_vector (4 downto 0);
       x3 : in bit_vector (4 downto 0);
       x4 : in bit_vector (4 downto 0);
       x5 : in bit_vector (4 downto 0);
       x6 : in bit_vector (4 downto 0);
       x7 : in bit_vector (4 downto 0);
       x8 : in bit_vector (4 downto 0);
       x9 : in bit_vector (4 downto 0);
       x10 : in bit_vector (4 downto 0);
       x11 : in bit_vector (4 downto 0);
       x12 : in bit_vector (4 downto 0);
       x13 : in bit_vector (4 downto 0);
       x14 : in bit_vector (4 downto 0);
       x15 : in bit_vector (4 downto 0);
       x16 : in bit_vector (4 downto 0);
       x17 : in bit_vector (4 downto 0);
       x18 : in bit_vector (4 downto 0);
       x19 : in bit_vector (4 downto 0);
       x20 : in bit_vector (4 downto 0);
       x21 : in bit_vector (4 downto 0);
       x22 : in bit_vector (4 downto 0);
       x23 : in bit_vector (4 downto 0);
       x24 : in bit_vector (4 downto 0);
       x25 : in bit_vector (4 downto 0);
       x26 : in bit_vector (4 downto 0);
       x27 : in bit_vector (4 downto 0);
       y : out bit_vector (4 downto 0));
end filter_mod_2p5m1;
architecture Behavioral of filter_mod_2p5m1 is
constant n : natural :=5;
signal p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, p26, p27, p28, p29, p30, p31, p32, p33, p34, p35, p36, p37, p38, p39, p40, p41, p42, p43, p44, p45, p46, p47 : bit_vector (n-1 downto 0);
signal sum1, sum2, sum3, sum4, sum5, sum6, sum7, sum8, sum9, sum10, sum11, sum12, sum13, sum14, sum15, sum16, sum17, sum18, sum19, sum20, sum21, sum22, sum23, sum24, sum25, sum26, sum27, sum28, sum29, sum30, sum31, sum32, sum33, sum34, sum35, sum36, sum37, sum38, sum39, sum40, sum41, sum42, sum43, sum44, sum45 : bit_vector (n-1 downto 0);
signal carry1, carry2, carry3, carry4, carry5, carry6, carry7, carry8, carry9, carry10, carry11, carry12, carry13, carry14, carry15, carry16, carry17, carry18, carry19, carry20, carry21, carry22, carry23, carry24, carry25, carry26, carry27, carry28, carry29, carry30, carry31, carry32, carry33, carry34, carry35, carry36, carry37, carry38, carry39, carry40, carry41, carry42, carry43, carry44, carry45 : bit_vector (n-1 downto 0);
begin
p1(0) <= not x1(4); p1(1) <= not x1(0); p1(2) <= not x1(1); p1(3) <= not x1(2); p1(4) <= not x1(3); 
p2(0) <= not x1(1); p2(1) <= not x1(2); p2(2) <= not x1(3); p2(3) <= not x1(4); p2(4) <= not x1(0); 
p3(0) <= x2(4); p3(1) <= x2(0); p3(2) <= x2(1); p3(3) <= x2(2); p3(4) <= x2(3); 
p4(0) <= x2(2); p4(1) <= x2(3); p4(2) <= x2(4); p4(3) <= x2(0); p4(4) <= x2(1); 
p5(0) <= not x3(4); p5(1) <= not x3(0); p5(2) <= not x3(1); p5(3) <= not x3(2); p5(4) <= not x3(3); 
p6(0) <= not x3(1); p6(1) <= not x3(2); p6(2) <= not x3(3); p6(3) <= not x3(4); p6(4) <= not x3(0); 
p7(0) <= not x4(0); p7(1) <= not x4(1); p7(2) <= not x4(2); p7(3) <= not x4(3); p7(4) <= not x4(4); 
p8(0) <= not x5(3); p8(1) <= not x5(4); p8(2) <= not x5(0); p8(3) <= not x5(1); p8(4) <= not x5(2); 
p9(0) <= not x5(1); p9(1) <= not x5(2); p9(2) <= not x5(3); p9(3) <= not x5(4); p9(4) <= not x5(0); 
p10(0) <= x6(4); p10(1) <= x6(0); p10(2) <= x6(1); p10(3) <= x6(2); p10(4) <= x6(3); 
p11(0) <= x6(1); p11(1) <= x6(2); p11(2) <= x6(3); p11(3) <= x6(4); p11(4) <= x6(0); 
p12(0) <= x7(0); p12(1) <= x7(1); p12(2) <= x7(2); p12(3) <= x7(3); p12(4) <= x7(4); 
p13(0) <= x7(3); p13(1) <= x7(4); p13(2) <= x7(0); p13(3) <= x7(1); p13(4) <= x7(2); 
p14(0) <= not x8(3); p14(1) <= not x8(4); p14(2) <= not x8(0); p14(3) <= not x8(1); p14(4) <= not x8(2); 
p15(0) <= not x8(2); p15(1) <= not x8(3); p15(2) <= not x8(4); p15(3) <= not x8(0); p15(4) <= not x8(1); 
p16(0) <= x9(4); p16(1) <= x9(0); p16(2) <= x9(1); p16(3) <= x9(2); p16(4) <= x9(3); 
p17(0) <= x9(1); p17(1) <= x9(2); p17(2) <= x9(3); p17(3) <= x9(4); p17(4) <= x9(0); 
p18(0) <= x10(0); p18(1) <= x10(1); p18(2) <= x10(2); p18(3) <= x10(3); p18(4) <= x10(4); 
p19(0) <= x10(3); p19(1) <= x10(4); p19(2) <= x10(0); p19(3) <= x10(1); p19(4) <= x10(2); 
p20(0) <= x11(3); p20(1) <= x11(4); p20(2) <= x11(0); p20(3) <= x11(1); p20(4) <= x11(2); 
p21(0) <= x11(2); p21(1) <= x11(3); p21(2) <= x11(4); p21(3) <= x11(0); p21(4) <= x11(1); 
p22(0) <= x12(1); p22(1) <= x12(2); p22(2) <= x12(3); p22(3) <= x12(4); p22(4) <= x12(0); 
p23(0) <= not x13(2); p23(1) <= not x13(3); p23(2) <= not x13(4); p23(3) <= not x13(0); p23(4) <= not x13(1); 
p24(0) <= not x13(1); p24(1) <= not x13(2); p24(2) <= not x13(3); p24(3) <= not x13(4); p24(4) <= not x13(0); 
p25(0) <= x14(4); p25(1) <= x14(0); p25(2) <= x14(1); p25(3) <= x14(2); p25(4) <= x14(3); 
p26(0) <= x14(3); p26(1) <= x14(4); p26(2) <= x14(0); p26(3) <= x14(1); p26(4) <= x14(2); 
p27(0) <= not x15(0); p27(1) <= not x15(1); p27(2) <= not x15(2); p27(3) <= not x15(3); p27(4) <= not x15(4); 
p28(0) <= x16(0); p28(1) <= x16(1); p28(2) <= x16(2); p28(3) <= x16(3); p28(4) <= x16(4); 
p29(0) <= x16(3); p29(1) <= x16(4); p29(2) <= x16(0); p29(3) <= x16(1); p29(4) <= x16(2); 
p30(0) <= x17(3); p30(1) <= x17(4); p30(2) <= x17(0); p30(3) <= x17(1); p30(4) <= x17(2); 
p31(0) <= x17(2); p31(1) <= x17(3); p31(2) <= x17(4); p31(3) <= x17(0); p31(4) <= x17(1); 
p32(0) <= x18(4); p32(1) <= x18(0); p32(2) <= x18(1); p32(3) <= x18(2); p32(4) <= x18(3); 
p33(0) <= x20(0); p33(1) <= x20(1); p33(2) <= x20(2); p33(3) <= x20(3); p33(4) <= x20(4); 
p34(0) <= x20(4); p34(1) <= x20(0); p34(2) <= x20(1); p34(3) <= x20(2); p34(4) <= x20(3); 
p35(0) <= x21(4); p35(1) <= x21(0); p35(2) <= x21(1); p35(3) <= x21(2); p35(4) <= x21(3); 
p36(0) <= x22(4); p36(1) <= x22(0); p36(2) <= x22(1); p36(3) <= x22(2); p36(4) <= x22(3); 
p37(0) <= x22(3); p37(1) <= x22(4); p37(2) <= x22(0); p37(3) <= x22(1); p37(4) <= x22(2); 
p38(0) <= not x23(3); p38(1) <= not x23(4); p38(2) <= not x23(0); p38(3) <= not x23(1); p38(4) <= not x23(2); 
p39(0) <= not x23(1); p39(1) <= not x23(2); p39(2) <= not x23(3); p39(3) <= not x23(4); p39(4) <= not x23(0); 
p40(0) <= not x24(0); p40(1) <= not x24(1); p40(2) <= not x24(2); p40(3) <= not x24(3); p40(4) <= not x24(4); 
p41(0) <= not x24(3); p41(1) <= not x24(4); p41(2) <= not x24(0); p41(3) <= not x24(1); p41(4) <= not x24(2); 
p42(0) <= x25(2); p42(1) <= x25(3); p42(2) <= x25(4); p42(3) <= x25(0); p42(4) <= x25(1); 
p43(0) <= x25(1); p43(1) <= x25(2); p43(2) <= x25(3); p43(3) <= x25(4); p43(4) <= x25(0); 
p44(0) <= x26(0); p44(1) <= x26(1); p44(2) <= x26(2); p44(3) <= x26(3); p44(4) <= x26(4); 
p45(0) <= x26(2); p45(1) <= x26(3); p45(2) <= x26(4); p45(3) <= x26(0); p45(4) <= x26(1); 
p46(0) <= not x27(3); p46(1) <= not x27(4); p46(2) <= not x27(0); p46(3) <= not x27(1); p46(4) <= not x27(2); 
p47 <= "11110";
EAC_CSA (p1, p2, p3, n, sum1, carry1);
EAC_CSA (p4, p5, p6, n, sum2, carry2);
EAC_CSA (p7, p8, p9, n, sum3, carry3);
EAC_CSA (p10, p11, p12, n, sum4, carry4);
EAC_CSA (p13, p14, p15, n, sum5, carry5);
EAC_CSA (p16, p17, p18, n, sum6, carry6);
EAC_CSA (p19, p20, p21, n, sum7, carry7);
EAC_CSA (p22, p23, p24, n, sum8, carry8);
EAC_CSA (p25, p26, p27, n, sum9, carry9);
EAC_CSA (p28, p29, p30, n, sum10, carry10);
EAC_CSA (p31, p32, p33, n, sum11, carry11);
EAC_CSA (p34, p35, p36, n, sum12, carry12);
EAC_CSA (p37, p38, p39, n, sum13, carry13);
EAC_CSA (p40, p41, p42, n, sum14, carry14);
EAC_CSA (p43, p44, p45, n, sum15, carry15);
EAC_CSA (sum1, carry1, sum2, n, sum16, carry16);
EAC_CSA (carry2, sum3, carry3, n, sum17, carry17);
EAC_CSA (sum4, carry4, sum5, n, sum18, carry18);
EAC_CSA (carry5, sum6, carry6, n, sum19, carry19);
EAC_CSA (sum7, carry7, sum8, n, sum20, carry20);
EAC_CSA (carry8, sum9, carry9, n, sum21, carry21);
EAC_CSA (sum10, carry10, sum11, n, sum22, carry22);
EAC_CSA (carry11, sum12, carry12, n, sum23, carry23);
EAC_CSA (sum13, carry13, sum14, n, sum24, carry24);
EAC_CSA (carry14, sum15, carry15, n, sum25, carry25);
EAC_CSA (sum16, carry16, sum17, n, sum26, carry26);
EAC_CSA (carry17, sum18, carry18, n, sum27, carry27);
EAC_CSA (sum19, carry19, sum20, n, sum28, carry28);
EAC_CSA (carry20, sum21, carry21, n, sum29, carry29);
EAC_CSA (sum22, carry22, sum23, n, sum30, carry30);
EAC_CSA (carry23, sum24, carry24, n, sum31, carry31);
EAC_CSA (sum25, carry25, p46, n, sum32, carry32);
EAC_CSA (sum26, carry26, sum27, n, sum33, carry33);
EAC_CSA (carry27, sum28, carry28, n, sum34, carry34);
EAC_CSA (sum29, carry29, sum30, n, sum35, carry35);
EAC_CSA (carry30, sum31, carry31, n, sum36, carry36);
EAC_CSA (sum33, carry33, sum34, n, sum37, carry37);
EAC_CSA (carry34, sum35, carry35, n, sum38, carry38);
EAC_CSA (sum36, carry36, sum32, n, sum39, carry39);
EAC_CSA (sum37, carry37, sum38, n, sum40, carry40);
EAC_CSA (carry38, sum39, carry39, n, sum41, carry41);
EAC_CSA (sum40, carry40, sum41, n, sum42, carry42);
EAC_CSA (sum42, carry42, carry41, n, sum43, carry43);
EAC_CSA (sum43, carry43, carry32, n, sum44, carry44);
EAC_CSA (sum44, carry44, p47, n, sum45, carry45);
y <= EAC_pref_add(sum45, carry45, n);
end Behavioral;
