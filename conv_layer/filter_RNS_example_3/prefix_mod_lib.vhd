--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.NUMERIC_BIT.all;

package prefix_mod_lib is

procedure first_stage (op1,op2 : in bit; h, g, p : out bit);
procedure h_first_stage (op1,op2 : in bit; h, g : out bit);
function third_stage (h, c : in bit) return bit;
procedure second_stage (g1, p1, g2, p2 : in bit; g,p : out bit);


function pref_add (op1, op2 : bit_vector; n: natural ) return bit_vector;
function mod_2n_pref_add (op1, op2 : bit_vector; n: natural ) return bit_vector;
function EAC_pref_add (op1, op2 : bit_vector; n: natural ) return bit_vector;

procedure HA  ( op1,op2 : bit; sum, not_carry : out bit);
procedure FA  ( op1,op2,c_in : bit; sum, c_out : out bit);


procedure CSA  ( op1,op2,op3 : in bit_vector; n : in natural; sum, carry : out bit_vector);
procedure CSA_mod2n  (signal op1,op2,op3 : in bit_vector; n : in natural;signal sum, carry : out bit_vector);
procedure EAC_CSA  (signal op1,op2,op3 : in bit_vector; n : in natural;signal sum, carry : out bit_vector);
procedure EAC_CSA_local  ( op1,op2,op3 : in bit_vector; n : in natural;  sum, carry : out bit_vector);

function mod_2pnm1 ( op1 : in bit_vector; n, k: in natural ) return bit_vector;
function mod_2pn ( op1 : in bit_vector; n: natural) return bit_vector;

function mul_m1 ( op1 : in bit_vector; n: natural) return bit_vector;

procedure Teq0 (op1, op2 : in bit; s, c : out bit);
procedure Teq1 (signal op1, op2 : in bit; signal s, c : out bit);
procedure adder_Teq0 (signal op1, op2 : in bit_vector;  n : in natural; signal s, c : out bit_vector);

procedure KSPPMA ( signal op1, op2, opt1, opt2 : in bit_vector; n, d, f: in natural;
							signal s, st : out bit_vector; signal carry : out bit);

end prefix_mod_lib;

package body prefix_mod_lib is

--stages------------------------------------------------------------

procedure first_stage (op1,op2 : in bit; h, g, p : out bit) is
begin
h:= op1 xor op2;
g:= op1 and op2;
p:= op1 or op2;
end first_stage;

procedure h_first_stage (op1,op2 : in bit; h, g : out bit) is
begin
h:= op1 xor op2;
g:= op1 and op2;
end h_first_stage;

procedure second_stage (g1, p1, g2, p2 : in bit; g,p : out bit)   is
begin
g:= g1 or (p1 and g2);
p:= p1 and p2;
end second_stage;

function h_second_stage (g1, p1, g2, p2 : in bit) return bit is
variable g : bit;
begin
g:= g1 or (p1 and g2);
return g;
end h_second_stage;

function third_stage (h, c : in bit) return  bit is
variable s : bit;
begin
s:= h xor c;
return s;
end third_stage;

--------------------------------------------------------------------

--SCSA(T)-----------------------------------------------------------
procedure Teq0 (op1, op2 : in bit; s, c : out bit) is
begin
s := op1 xor op2;
c := op1 and op2;
end Teq0;

procedure Teq1 (signal op1, op2 : in bit;signal s, c : out bit) is
--variable sum, carry : bit;
begin
s <= op1 xnor op2;
c <= op1 or op2;
--s <= sum;
--c <= carry;
end Teq1;

procedure adder_Teq0 (signal op1, op2 : in bit_vector; n : in natural; signal s, c : out bit_vector) is
variable sum : bit_vector (n-1 downto 0);
variable carry : bit_vector (n downto 0);
begin
carry(0) := '0';
for i in 0 to n-1
	loop
	Teq0(op1(i),op2(i),sum(i),carry(i+1));
	end loop;
s <= sum;
c <= carry;
end adder_Teq0;
--------------------------------------------------------------------

--DASH-CPA----------------------------------------------------------
--KSPPMA------------------------------------------------------------
procedure KSPPMA ( signal op1, op2, opt1, opt2 : in bit_vector; n, d, f: in natural;
							signal s, st : out bit_vector; signal carry : out bit) is
variable sum : bit_vector (n-1 downto 0);
variable sumt : bit_vector (n-1 downto 0);
variable c : bit;
variable h, g, p : bit_vector (n-1 downto 0);
variable ht, gt, pt : bit_vector (n-1 downto 0);
variable k : natural := 1;
begin

h_first_stage(op1(1),op2(1),h(1),g(1));
h_first_stage(opt1(1),opt2(1),ht(1),gt(1));
for i in 2 to n-1
	loop
		first_stage(op1(i),op2(i),h(i),g(i),p(i));
		if i < d then
			first_stage(opt1(i),opt2(i),ht(i),gt(i),pt(i));
		end if;
	end loop;
	
while (k < n-1)
	loop
		if (k = 1) then
			second_stage(g(n-1),p(n-1),g(n-1-k),p(n-1-k),gt(n-1),pt(n-1));
			for j in n-2 downto d
				loop
					second_stage(g(j),p(j),g(j-k),p(j-k),g(j),p(j));
					if (j = d) then
						second_stage(g(j),p(j),gt(j-k),pt(j-k),gt(j),pt(j));
					else
						second_stage(g(j),p(j),g(j-k),p(j-k),gt(j),pt(j));
					end if;
				end loop;
			
		else
			gt(n-1) := h_second_stage(gt(n-1),pt(n-1),gt(n-1-k),pt(n-1-k));
			for j in n-2 downto k
				loop
					g(j) := h_second_stage(g(j),p(j),g(j-k),p(j-k));
					gt(j) := h_second_stage(gt(j),pt(j),gt(j-k),pt(j-k));
				end loop;
		end if;
		k := k*2;
	end loop;	
sum(0):=op1(0);
sumt(0):=opt1(0);
sum(1):=h(1);
sumt(1):=ht(1);
for i in 2 to n-1
	loop
		sum(i):=third_stage(h(i),g(i-1));
		if (i >= d) then
			sumt(i):=third_stage(h(i),gt(i-1));
		else
			sumt(i):=third_stage(ht(i),gt(i-1));
		end if;
	end loop;		
c := op2(n) or gt(n-1);
s <= sum;
st <= sumt;	
carry <= c;
end KSPPMA;
--------------------------------------------------------------------

--prefix adders-----------------------------------------------------

function pref_add (op1, op2 : bit_vector; n: natural ) return bit_vector is
variable sum : bit_vector (n downto 0);
variable h, g, p : bit_vector (n-1 downto 0);
variable k : natural := 1;
begin
for i in 0 to n-1
	loop
	first_stage(op1(i),op2(i),h(i),g(i),p(i));
	end loop;
while (k < n-1)
	loop
	for j in n-1 downto k
		loop
		second_stage(g(j),p(j),g(j-k),p(j-k),g(j),p(j));
		end loop;
	k := k*2;
	end loop;	
for i in 1 to n-1
	loop
	sum(i):=third_stage(h(i),g(i-1));
	end loop;
sum(0):=h(0);		
sum(n):=g(n-1);
return sum;	
end pref_add;

--------------------------------------------------------------------
--mod 2^n prefix adders---------------------------------------------------
function mod_2n_pref_add (op1, op2 : bit_vector; n: natural ) return bit_vector is
variable h, g, p, sum : bit_vector (n-1 downto 0);
variable k : natural := 1;
begin
for i in 0 to n-1
	loop
	first_stage(op1(i),op2(i),h(i),g(i),p(i));
	end loop;
while (k < n-1)
	loop
	for j in n-1 downto k
		loop
		second_stage(g(j),p(j),g(j-k),p(j-k),g(j),p(j));
		end loop;
	k := k*2;
	end loop;	
for i in 1 to n-1
	loop
	sum(i):=third_stage(h(i),g(i-1));
	end loop;
sum(0):=h(0);		
return sum;	
end  mod_2n_pref_add;
--------------------------------------------------------------------
--prefix adders - end around carry----------------------------------------
function EAC_pref_add (op1, op2 : bit_vector; n: natural ) return bit_vector is
variable h, g, p, sum : bit_vector (n-1 downto 0);
variable k : natural := 1;
begin
for i in 0 to n-1
	loop
	first_stage(op1(i),op2(i),h(i),g(i),p(i));
	end loop;
while (k < n-1)
	loop
	for j in 0 to k-1
		loop
		second_stage(g(j),p(j),g(j+n-k),p(j+n-k),g(j),p(j));
		end loop;
	for j in n-1 downto k
		loop
		second_stage(g(j),p(j),g(j-k),p(j-k),g(j),p(j));
		end loop;
	k := k*2;
	end loop;	
for i in 1 to n-1
	loop
	sum(i):=third_stage(h(i),g(i-1));
	end loop;
sum(0):=third_stage(h(0),g(n-1));		
return sum;	
end EAC_pref_add;
--------------------------------------------------------------------

--mod prefix adders---------------------------------------------------
function mod_pref_add (op1, op2, t : bit_vector; n: natural ) return bit_vector is
variable h, g, p, sum : bit_vector (n-1 downto 0);
variable k : natural := 1;
begin
for i in 0 to n-1
	loop
	first_stage(op1(i),op2(i),h(i),g(i),p(i));
	end loop;
while (k < n-1)
	loop
	for j in n-1 downto k
		loop
		second_stage(g(j),p(j),g(j-k),p(j-k),g(j),p(j));
		end loop;
	k := k*2;
	end loop;	
for i in 1 to n-1
	loop
	sum(i):=third_stage(h(i),g(i-1));
	end loop;
sum(0):=h(0);		
return sum;	
end  mod_pref_add;
--------------------------------------------------------------------

--Half-adder--------------------------------------------------------
procedure HA  ( op1,op2 : bit; sum, not_carry : out bit) is
variable nc: bit;
begin
--not carry
nc := op1 nand op2;
not_carry:= nc;
--sum
sum:=(nc nand op1) nand (nc nand op2);
end HA;
--------------------------------------------------------------------
--full-adder--------------------------------------------------------
procedure FA  ( op1,op2,c_in : bit; sum, c_out : out bit) is
variable sum_tmp,not_c_tmp1,not_c_tmp2: bit;
begin
HA(op1,op2,sum_tmp,not_c_tmp1);
--sum
HA(sum_tmp,c_in,sum,not_c_tmp2);
--carry
c_out:=not_c_tmp1 nand not_c_tmp2;
end FA;
--------------------------------------------------------------------
--Carry Save Adder -------------------------------------------------
procedure CSA  (op1,op2,op3 : in bit_vector; n : in natural; sum, carry : out bit_vector) is
variable s : bit_vector(n-1 downto 0);
variable c_out : bit_vector(n downto 0);
begin
for i in 0 to n-2 
	loop
	FA(op1(i),op2(i),op3(i),s(i),c_out(i+1));
	end loop;
--n and n-1 bits
FA(op1(n-1),op2(n-1),op3(n-1),s(n-1),c_out(n));
sum:=s;
carry:=c_out;
end CSA;

procedure CSA_mod2n  (signal op1,op2,op3 : in bit_vector; n : in natural;signal sum, carry : out bit_vector) is
variable s : bit_vector(n-1 downto 0);
variable c_out : bit_vector(n downto 0);
begin
for i in 0 to n-2 
	loop
	FA(op1(i),op2(i),op3(i),s(i),c_out(i+1));
	end loop;
--n and n-1 bits
FA(op1(n-1),op2(n-1),op3(n-1),s(n-1),c_out(n));
sum<=s;
carry<=c_out (n-1 downto 0);
end CSA_mod2n;
--------------------------------------------------------------------

--End Around Carry - Carry Save Adder-------------------------------
procedure EAC_CSA  (signal op1,op2,op3 : in bit_vector; n : in natural;signal  sum, carry : out bit_vector) is
variable s, c_out : bit_vector(n-1 downto 0);
begin
for i in 0 to n-2 
	loop
	FA(op1(i),op2(i),op3(i),s(i),c_out(i+1));
	end loop;
--0 and n-1 bits
FA(op1(n-1),op2(n-1),op3(n-1),s(n-1),c_out(0));
sum<=s;
carry<=c_out;
end EAC_CSA;

procedure EAC_CSA_local  ( op1,op2,op3 : in bit_vector; n : in natural;  sum, carry : out bit_vector) is
variable s, c_out : bit_vector(n-1 downto 0);
begin
for i in 0 to n-2 
	loop
	FA(op1(i),op2(i),op3(i),s(i),c_out(i+1));
	end loop;
--0 and n-1 bits
FA(op1(n-1),op2(n-1),op3(n-1),s(n-1),c_out(0));
sum:=s;
carry:=c_out;
end EAC_CSA_local;
--------------------------------------------------------------------

--BNS to RNS--------------------------------------------------------
--2^n-1-------

function mod_2pnm1 ( op1 : in bit_vector; n, k: in natural ) return bit_vector is
variable tmp_1, tmp_2, tmp_3 : bit_vector (n-1 downto 0);  
variable ans : bit_vector (n-1 downto 0); 
begin
	tmp_1 := op1(n-1 downto 0);
if (k/n >= 2)then
	tmp_2 := op1(2*n-1 downto n);	
	if (k/n >= 3) then
		for i in 3 to k/n
			loop
			tmp_3 := op1(n*i-1 downto (i-1)*n);
			EAC_CSA_local(tmp_1, tmp_2, tmp_3, n, tmp_1, tmp_2);
			end loop;
	end if;
	if (k/n*n < k) then
		tmp_3 := (others =>'0');
		tmp_3(k-k/n*n-1 downto 0) := op1(k-1 downto k/n*n);
		EAC_CSA_local(tmp_1, tmp_2, tmp_3, n, tmp_1, tmp_2);
	end if;
else
	tmp_2(k-k/n*n-1 downto 0) := op1(k-1 downto k/n*n);
end if;
ans := EAC_pref_add (tmp_1, tmp_2, n);
return ans;
end mod_2pnm1;

--------------

--2^n---------
function mod_2pn ( op1 : in bit_vector; n: natural) return bit_vector is
variable ans : bit_vector (n-1 downto 0); 
begin
ans := op1 (n-1 downto 0);
return ans;
end mod_2pn;
--------------
--mul_-1-------
function mul_m1 ( op1 : in bit_vector; n: natural) return bit_vector is
variable ans, one : bit_vector (n-1 downto 0); 
begin
one(0) := '1';
ans := mod_2n_pref_add (not op1, one, n);
return ans;
end mul_m1;
---------------


end prefix_mod_lib;
