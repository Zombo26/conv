library IEEE;
use IEEE.numeric_bit.ALL;
use work.prefix_mod_lib.all;
entity rns_to_pss is
   Port (
       y1 : in bit_vector (2 downto 0);
       y2 : in bit_vector (3 downto 0);
       y3 : in bit_vector (4 downto 0);
       y4 : in bit_vector (8 downto 0);
       z : out bit_vector (20 downto 0));------------------------------------------
end rns_to_pss;
architecture Behavioral of rns_to_pss is
constant n : natural :=29;
signal p1, p2, p3, p4, p5, p6, p7, p8 : bit_vector (n-1 downto 0);
signal sum1, sum2, sum3, sum4, sum5, sum6 : bit_vector (n-1 downto 0);
signal carry1, carry2, carry3, carry4, carry5,carry6 : bit_vector (n-1 downto 0);
signal rez : bit_vector (n-1 downto 0);

constant k : natural :=50;
signal p9, p10, p11, p12, p13, p14, p15, p16 : bit_vector (k-1 downto 0);
signal sum7, sum8, sum9, sum10, sum11, sum12 : bit_vector (k-1 downto 0);
signal carry7, carry8, carry9, carry10, carry11, carry12 : bit_vector (k-1 downto 0);
signal rez_2 : bit_vector (k-1 downto 0);

begin
p1(0) <= not y1(0); p1(1) <= not y1(1); p1(2) <= not y1(2); p1(3) <= not y1(0); p1(4) <= not y1(1); p1(5) <= not y1(2); p1(6) <= not y1(0); p1(7) <= not y1(1); p1(8) <= not y1(2); p1(9) <= not y1(0); p1(10) <= not y1(1); p1(11) <= not y1(2); p1(12) <= not y1(0); p1(13) <= not y1(1); p1(14) <= not y1(2); p1(15) <= not y1(0); p1(16) <= not y1(1); p1(17) <= not y1(2); p1(18) <= not y1(0); p1(19) <= not y1(1); p1(20) <= not y1(2); p1(21) <= not y1(0); p1(22) <= not y1(1); p1(23) <= not y1(2); p1(24) <= not y1(0); p1(25) <= not y1(1); p1(26) <= not y1(2); p1(27) <= not y1(0); p1(28) <= not y1(1); 
p2(0) <= '1'; p2(1) <= not y2(0); p2(2) <= not y2(1); p2(3) <= not y2(2); p2(4) <= not y2(3); p2(5) <= not y2(0); p2(6) <= not y2(1); p2(7) <= not y2(2); p2(8) <= not y2(3); p2(9) <= not y2(0); p2(10) <= not y2(1); p2(11) <= not y2(2); p2(12) <= not y2(3); p2(13) <= not y2(0); p2(14) <= not y2(1); p2(15) <= not y2(2); p2(16) <= not y2(3); p2(17) <= not y2(0); p2(18) <= not y2(1); p2(19) <= not y2(2); p2(20) <= not y2(3); p2(21) <= not y2(0); p2(22) <= not y2(1); p2(23) <= not y2(2); p2(24) <= not y2(3); p2(25) <= not y2(0); p2(26) <= not y2(1); p2(27) <= not y2(2); p2(28) <= not y2(3); 
p3(1) <= not y3(0); p3(2) <= not y3(1); p3(3) <= not y3(2); p3(4) <= not y3(3); p3(5) <= not y3(4); p3(6) <= not y3(2); p3(7) <= not y3(3); p3(8) <= not y3(4); p3(9) <= not y3(3); p3(10) <= not y3(4); p3(11) <= not y3(2); p3(12) <= not y3(3); p3(13) <= not y3(4); p3(14) <= not y3(3); p3(15) <= not y3(4); p3(16) <= not y3(2); p3(17) <= not y3(3); p3(18) <= not y3(4); p3(19) <= not y3(3); p3(20) <= not y3(4); p3(21) <= not y3(2); p3(22) <= not y3(3); p3(23) <= not y3(4); p3(24) <= not y3(3); p3(25) <= not y3(4); p3(26) <= not y3(2); p3(27) <= not y3(3); p3(28) <= not y3(4); 
p4(2) <= '1'; p4(4) <= not y3(0); p4(5) <= not y3(1); p4(6) <= not y3(0); p4(7) <= not y3(1); p4(8) <= not y3(2); p4(9) <= not y3(0); p4(10) <= not y3(1); p4(11) <= not y3(0); p4(12) <= not y3(1); p4(13) <= not y3(2); p4(14) <= not y3(0); p4(15) <= not y3(1); p4(16) <= not y3(0); p4(17) <= not y3(1); p4(18) <= not y3(2); p4(19) <= not y3(0); p4(20) <= not y3(1); p4(21) <= not y3(0); p4(22) <= not y3(1); p4(23) <= not y3(2); p4(24) <= not y3(0); p4(25) <= not y3(1); p4(26) <= not y3(0); p4(27) <= not y3(1); p4(28) <= not y3(2); 
p5(4) <= '1'; p5(20) <= y4(0); p5(21) <= y4(1); p5(22) <= y4(2); p5(23) <= y4(3); p5(24) <= y4(4); p5(25) <= y4(5); p5(26) <= y4(6); p5(27) <= y4(7); p5(28) <= y4(8); 
p6(21) <= y4(0); p6(22) <= y4(1); p6(23) <= y4(2); p6(24) <= y4(3); p6(25) <= y4(4); p6(26) <= y4(5); p6(27) <= y4(6); p6(28) <= y4(7); 
p7(22) <= y4(0); p7(23) <= y4(1); p7(24) <= y4(2); p7(25) <= y4(3); p7(26) <= y4(4); p7(27) <= y4(5); p7(28) <= y4(6);
p8(28) <= y4(0); 
CSA_mod2n (p1, p2, p3, n, sum1, carry1);
CSA_mod2n (p4, p5, p6, n, sum2, carry2);
CSA_mod2n (sum1, carry1, sum2, n, sum3, carry3);
CSA_mod2n (carry2, p7, p8, n, sum4, carry4);
CSA_mod2n (sum3, carry3, sum4, n, sum5, carry5);
CSA_mod2n (sum5, carry5, carry4, n, sum6, carry6);
rez <= mod_2n_pref_add(sum6, carry6, n);
--------------------------------------------------------------

p9(9) <= rez(0); p9(10) <= rez(1); p9(11) <= rez(2); p9(12) <= rez(3); p9(13) <= rez(4); p9(14) <= rez(5); p9(15) <= rez(6); p9(16) <= rez(7); p9(17) <= rez(8); p9(18) <= rez(9); p9(19) <= rez(10); p9(20) <= rez(11); p9(21) <= rez(12); p9(22) <= rez(13); p9(23) <= rez(14); p9(24) <= rez(15); p9(25) <= rez(16); p9(26) <= rez(17); p9(27) <= rez(18); p9(28) <= rez(19); p9(29) <= rez(20); p9(30) <= rez(21); p9(31) <= rez(22); p9(32) <= rez(23); p9(33) <= rez(24); p9(34) <= rez(25); p9(35) <= rez(26); p9(36) <= rez(27); p9(37) <= rez(28); p9(38) <= rez(28); p9(39) <= rez(28); p9(40) <= rez(27); p9(41) <= rez(28); p9(42) <= rez(28); p9(43) <= rez(27); p9(44) <= rez(28); p9(45) <= rez(26); p9(46) <= rez(27); p9(47) <= rez(28); p9(48) <= rez(28); 
p10(10) <= rez(0); p10(11) <= rez(1); p10(12) <= rez(2); p10(13) <= rez(3); p10(14) <= rez(4); p10(15) <= rez(5); p10(16) <= rez(6); p10(17) <= rez(7); p10(18) <= rez(8); p10(19) <= rez(9); p10(20) <= rez(10); p10(21) <= rez(11); p10(22) <= rez(12); p10(23) <= rez(13); p10(24) <= rez(14); p10(25) <= rez(15); p10(26) <= rez(16); p10(27) <= rez(17); p10(28) <= rez(18); p10(29) <= rez(19); p10(30) <= rez(20); p10(31) <= rez(21); p10(32) <= rez(22); p10(33) <= rez(23); p10(34) <= rez(24); p10(35) <= rez(25); p10(36) <= rez(26); p10(37) <= rez(27); p10(38) <= rez(27); p10(39) <= rez(26); p10(40) <= rez(26); p10(41) <= rez(27); p10(42) <= rez(26); p10(43) <= rez(24); p10(44) <= rez(25); p10(45) <= rez(25); p10(46) <= rez(26); p10(47) <= rez(27); 
p11(11) <= rez(0); p11(12) <= rez(1); p11(13) <= rez(2); p11(14) <= rez(3); p11(15) <= rez(4); p11(16) <= rez(5); p11(17) <= rez(6); p11(18) <= rez(7); p11(19) <= rez(8); p11(20) <= rez(9); p11(21) <= rez(10); p11(22) <= rez(11); p11(23) <= rez(12); p11(24) <= rez(13); p11(25) <= rez(14); p11(26) <= rez(15); p11(27) <= rez(16); p11(28) <= rez(17); p11(29) <= rez(18); p11(30) <= rez(19); p11(31) <= rez(20); p11(32) <= rez(21); p11(33) <= rez(22); p11(34) <= rez(23); p11(35) <= rez(24); p11(36) <= rez(25); p11(37) <= rez(26); p11(38) <= rez(25); p11(39) <= rez(25); p11(40) <= rez(24); p11(41) <= rez(25); p11(42) <= rez(23); p11(43) <= rez(23); p11(44) <= rez(24); 
p12(13) <= rez(0); p12(14) <= rez(1); p12(15) <= rez(2); p12(16) <= rez(3); p12(17) <= rez(4); p12(18) <= rez(5); p12(19) <= rez(6); p12(20) <= rez(7); p12(21) <= rez(8); p12(22) <= rez(9); p12(23) <= rez(10); p12(24) <= rez(11); p12(25) <= rez(12); p12(26) <= rez(13); p12(27) <= rez(14); p12(28) <= rez(15); p12(29) <= rez(16); p12(30) <= rez(17); p12(31) <= rez(18); p12(32) <= rez(19); p12(33) <= rez(20); p12(34) <= rez(21); p12(35) <= rez(22); p12(36) <= rez(23); p12(37) <= rez(24); p12(38) <= rez(24); p12(39) <= rez(23); p12(40) <= rez(21); p12(41) <= rez(22); p12(42) <= rez(22); 
p13(14) <= rez(0); p13(15) <= rez(1); p13(16) <= rez(2); p13(17) <= rez(3); p13(18) <= rez(4); p13(19) <= rez(5); p13(20) <= rez(6); p13(21) <= rez(7); p13(22) <= rez(8); p13(23) <= rez(9); p13(24) <= rez(10); p13(25) <= rez(11); p13(26) <= rez(12); p13(27) <= rez(13); p13(28) <= rez(14); p13(29) <= rez(15); p13(30) <= rez(16); p13(31) <= rez(17); p13(32) <= rez(18); p13(33) <= rez(19); p13(34) <= rez(20); p13(35) <= rez(21); p13(36) <= rez(22); p13(37) <= rez(23); p13(38) <= rez(22); p13(39) <= rez(20); p13(40) <= rez(20); p13(41) <= rez(21); 
p14(16) <= rez(0); p14(17) <= rez(1); p14(18) <= rez(2); p14(19) <= rez(3); p14(20) <= rez(4); p14(21) <= rez(5); p14(22) <= rez(6); p14(23) <= rez(7); p14(24) <= rez(8); p14(25) <= rez(9); p14(26) <= rez(10); p14(27) <= rez(11); p14(28) <= rez(12); p14(29) <= rez(13); p14(30) <= rez(14); p14(31) <= rez(15); p14(32) <= rez(16); p14(33) <= rez(17); p14(34) <= rez(18); p14(35) <= rez(19); p14(36) <= rez(20); p14(37) <= rez(21); p14(38) <= rez(19); p14(39) <= rez(19); 
p15(19) <= rez(0); p15(20) <= rez(1); p15(21) <= rez(2); p15(22) <= rez(3); p15(23) <= rez(4); p15(24) <= rez(5); p15(25) <= rez(6); p15(26) <= rez(7); p15(27) <= rez(8); p15(28) <= rez(9); p15(29) <= rez(10); p15(30) <= rez(11); p15(31) <= rez(12); p15(32) <= rez(13); p15(33) <= rez(14); p15(34) <= rez(15); p15(35) <= rez(16); p15(36) <= rez(17); p15(37) <= rez(18); p15(38) <= rez(18); 
p16(20) <= rez(0); p16(21) <= rez(1); p16(22) <= rez(2); p16(23) <= rez(3); p16(24) <= rez(4); p16(25) <= rez(5); p16(26) <= rez(6); p16(27) <= rez(7); p16(28) <= rez(8); p16(29) <= rez(9); p16(30) <= rez(10); p16(31) <= rez(11); p16(32) <= rez(12); p16(33) <= rez(13); p16(34) <= rez(14); p16(35) <= rez(15); p16(36) <= rez(16); p16(37) <= rez(17); 
CSA_mod2n (p9, p10, p11, k, sum7, carry7);
CSA_mod2n (p12, p13, p14, k, sum8, carry8);
CSA_mod2n (sum7, carry7, sum8, k, sum9, carry9);
CSA_mod2n (carry8, p15, p16, k, sum10, carry10);
CSA_mod2n (sum9, carry9, sum10, k, sum11, carry11);
CSA_mod2n (sum11, carry11, carry10, k, sum12, carry12);
rez_2 <= mod_2n_pref_add(sum12, carry12, k);

z <= rez_2(k-1 downto n);
end Behavioral;
