----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:49:30 09/01/2017 
-- Design Name: 
-- Module Name:    gauss - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.numeric_bit.ALL;

entity filter is
    Port ( r_a1 : in  bit_vector (7 downto 0);
           r_a2 : in  bit_vector (7 downto 0);
           r_a3 : in  bit_vector (7 downto 0);
           r_a4 : in  bit_vector (7 downto 0);
           r_a5 : in  bit_vector (7 downto 0);
           r_a6 : in  bit_vector (7 downto 0);
           r_a7 : in  bit_vector (7 downto 0);
           r_a8 : in  bit_vector (7 downto 0);
           r_a9 : in  bit_vector (7 downto 0);
		   
		     g_a1 : in  bit_vector (7 downto 0);
           g_a2 : in  bit_vector (7 downto 0);
           g_a3 : in  bit_vector (7 downto 0);
           g_a4 : in  bit_vector (7 downto 0);
           g_a5 : in  bit_vector (7 downto 0);
           g_a6 : in  bit_vector (7 downto 0);
           g_a7 : in  bit_vector (7 downto 0);
           g_a8 : in  bit_vector (7 downto 0);
           g_a9 : in  bit_vector (7 downto 0);
		   
		     b_a1 : in  bit_vector (7 downto 0);
           b_a2 : in  bit_vector (7 downto 0);
           b_a3 : in  bit_vector (7 downto 0);
           b_a4 : in  bit_vector (7 downto 0);
           b_a5 : in  bit_vector (7 downto 0);
           b_a6 : in  bit_vector (7 downto 0);
           b_a7 : in  bit_vector (7 downto 0);
           b_a8 : in  bit_vector (7 downto 0);
           b_a9 : in  bit_vector (7 downto 0);			  
		   
           f : out  bit_vector (13 downto 0));-----------------------------------
end filter;

architecture Behavioral of filter is

component pss_to_rns
    Port ( x : in  bit_vector (7 downto 0);
		   y1 : out  bit_vector (2 downto 0);
           y2 : out  bit_vector (3 downto 0);
           y3 : out  bit_vector (4 downto 0);
           y4 : out  bit_vector (8 downto 0));
end component;

component filter_mod_2p4m1 
    Port ( x1 : in  bit_vector (3 downto 0);
			  x2 : in  bit_vector (3 downto 0);
			  x3 : in  bit_vector (3 downto 0);
			  x4 : in  bit_vector (3 downto 0);
			  x5 : in  bit_vector (3 downto 0);
			  x6 : in  bit_vector (3 downto 0);
			  x7 : in  bit_vector (3 downto 0);
			  x8 : in  bit_vector (3 downto 0);
			  x9 : in  bit_vector (3 downto 0);
			  
			  x10 : in  bit_vector (3 downto 0);
			  x11 : in  bit_vector (3 downto 0);
			  x12 : in  bit_vector (3 downto 0);
			  x13 : in  bit_vector (3 downto 0);
			  x14 : in  bit_vector (3 downto 0);
			  x15 : in  bit_vector (3 downto 0);
			  x16 : in  bit_vector (3 downto 0);
			  x17 : in  bit_vector (3 downto 0);
			  x18 : in  bit_vector (3 downto 0);
			  
			  x19 : in  bit_vector (3 downto 0);
			  x20 : in  bit_vector (3 downto 0);
			  x21 : in  bit_vector (3 downto 0);
			  x22 : in  bit_vector (3 downto 0);
			  x23 : in  bit_vector (3 downto 0);
			  x24 : in  bit_vector (3 downto 0);
			  x25 : in  bit_vector (3 downto 0);
			  x26 : in  bit_vector (3 downto 0);
			  x27 : in  bit_vector (3 downto 0);
           y : out  bit_vector (3 downto 0));
end component;

component filter_mod_2p3m1 
    Port ( x1 : in  bit_vector (2 downto 0);
			  x2 : in  bit_vector (2 downto 0);
			  x3 : in  bit_vector (2 downto 0);
			  x4 : in  bit_vector (2 downto 0);
			  x5 : in  bit_vector (2 downto 0);
			  x6 : in  bit_vector (2 downto 0);
			  x7 : in  bit_vector (2 downto 0);
			  x8 : in  bit_vector (2 downto 0);
			  x9 : in  bit_vector (2 downto 0);
			  
			  x10 : in  bit_vector (2 downto 0);
			  x11 : in  bit_vector (2 downto 0);
			  x12 : in  bit_vector (2 downto 0);
			  x13 : in  bit_vector (2 downto 0);
			  x14 : in  bit_vector (2 downto 0);
			  x15 : in  bit_vector (2 downto 0);
			  x16 : in  bit_vector (2 downto 0);
			  x17 : in  bit_vector (2 downto 0);
			  x18 : in  bit_vector (2 downto 0);
			  
			  x19 : in  bit_vector (2 downto 0);
			  x20 : in  bit_vector (2 downto 0);
			  x21 : in  bit_vector (2 downto 0);
			  x22 : in  bit_vector (2 downto 0);
			  x23 : in  bit_vector (2 downto 0);
			  x24 : in  bit_vector (2 downto 0);
			  x25 : in  bit_vector (2 downto 0);
			  x26 : in  bit_vector (2 downto 0);
			  x27 : in  bit_vector (2 downto 0);
           y : out  bit_vector (2 downto 0));
end component;

component filter_mod_2p5m1
    Port ( x1 : in  bit_vector (4 downto 0);
           x2 : in  bit_vector (4 downto 0);
           x3 : in  bit_vector (4 downto 0);
           x4 : in  bit_vector (4 downto 0);
           x5 : in  bit_vector (4 downto 0);
           x6 : in  bit_vector (4 downto 0);
           x7 : in  bit_vector (4 downto 0);
           x8 : in  bit_vector (4 downto 0);
           x9 : in  bit_vector (4 downto 0);
		   
		     x10 : in  bit_vector (4 downto 0);
			  x11 : in  bit_vector (4 downto 0);
			  x12 : in  bit_vector (4 downto 0);
			  x13 : in  bit_vector (4 downto 0);
			  x14 : in  bit_vector (4 downto 0);
			  x15 : in  bit_vector (4 downto 0);
			  x16 : in  bit_vector (4 downto 0);
			  x17 : in  bit_vector (4 downto 0);
			  x18 : in  bit_vector (4 downto 0);
			  
			  x19 : in  bit_vector (4 downto 0);
			  x20 : in  bit_vector (4 downto 0);
			  x21 : in  bit_vector (4 downto 0);
			  x22 : in  bit_vector (4 downto 0);
			  x23 : in  bit_vector (4 downto 0);
			  x24 : in  bit_vector (4 downto 0);
			  x25 : in  bit_vector (4 downto 0);
			  x26 : in  bit_vector (4 downto 0);
			  x27 : in  bit_vector (4 downto 0);
           y : out  bit_vector (4 downto 0));
end component;

component filter_mod_2p9
    Port ( x1 : in  bit_vector (8 downto 0);
           x2 : in  bit_vector (8 downto 0);
           x3 : in  bit_vector (8 downto 0);
           x4 : in  bit_vector (8 downto 0);
           x5 : in  bit_vector (8 downto 0);
           x6 : in  bit_vector (8 downto 0);
           x7 : in  bit_vector (8 downto 0);
           x8 : in  bit_vector (8 downto 0);
           x9 : in  bit_vector (8 downto 0);
		   
		     x10 : in  bit_vector (8 downto 0);
			  x11 : in  bit_vector (8 downto 0);
			  x12 : in  bit_vector (8 downto 0);
			  x13 : in  bit_vector (8 downto 0);
			  x14 : in  bit_vector (8 downto 0);
			  x15 : in  bit_vector (8 downto 0);
			  x16 : in  bit_vector (8 downto 0);
			  x17 : in  bit_vector (8 downto 0);
			  x18 : in  bit_vector (8 downto 0);
			  
			  x19 : in  bit_vector (8 downto 0);
			  x20 : in  bit_vector (8 downto 0);
			  x21 : in  bit_vector (8 downto 0);
			  x22 : in  bit_vector (8 downto 0);
			  x23 : in  bit_vector (8 downto 0);
			  x24 : in  bit_vector (8 downto 0);
			  x25 : in  bit_vector (8 downto 0);
			  x26 : in  bit_vector (8 downto 0);
			  x27 : in  bit_vector (8 downto 0);
           y : out  bit_vector (8 downto 0));
end component;

component rns_to_pss
    Port ( y1 : in  bit_vector (2 downto 0);
		   y2 : in  bit_vector (3 downto 0);
           y3 : in  bit_vector (4 downto 0);
           y4 : in  bit_vector (8 downto 0);
           z : out  bit_vector (20 downto 0));----------------------------
end component;

signal a1_m31 :  bit_vector (2 downto 0);
signal a1_m41 :  bit_vector (3 downto 0);
signal a1_m51 :  bit_vector (4 downto 0);
signal a1_m9 :  bit_vector (8 downto 0);
signal a2_m31 :  bit_vector (2 downto 0);
signal a2_m41 :  bit_vector (3 downto 0);
signal a2_m51 :  bit_vector (4 downto 0);
signal a2_m9 :  bit_vector (8 downto 0);
signal a3_m31 :  bit_vector (2 downto 0);
signal a3_m41 :  bit_vector (3 downto 0);
signal a3_m51 :  bit_vector (4 downto 0);
signal a3_m9 :  bit_vector (8 downto 0);
signal a4_m31 :  bit_vector (2 downto 0);
signal a4_m41 :  bit_vector (3 downto 0);
signal a4_m51 :  bit_vector (4 downto 0);
signal a4_m9 :  bit_vector (8 downto 0);
signal a5_m31 :  bit_vector (2 downto 0);
signal a5_m41 :  bit_vector (3 downto 0);
signal a5_m51 :  bit_vector (4 downto 0);
signal a5_m9 :  bit_vector (8 downto 0);
signal a6_m31 :  bit_vector (2 downto 0);
signal a6_m41 :  bit_vector (3 downto 0);
signal a6_m51 :  bit_vector (4 downto 0);
signal a6_m9 :  bit_vector (8 downto 0);
signal a7_m31 :  bit_vector (2 downto 0);
signal a7_m41 :  bit_vector (3 downto 0);
signal a7_m51 :  bit_vector (4 downto 0);
signal a7_m9 :  bit_vector (8 downto 0);
signal a8_m31 :  bit_vector (2 downto 0);
signal a8_m41 :  bit_vector (3 downto 0);
signal a8_m51 :  bit_vector (4 downto 0);
signal a8_m9 :  bit_vector (8 downto 0);
signal a9_m31 :  bit_vector (2 downto 0);
signal a9_m41 :  bit_vector (3 downto 0);
signal a9_m51 :  bit_vector (4 downto 0);
signal a9_m9 :  bit_vector (8 downto 0);
signal a10_m31 :  bit_vector (2 downto 0);
signal a10_m41 :  bit_vector (3 downto 0);
signal a10_m51 :  bit_vector (4 downto 0);
signal a10_m9 :  bit_vector (8 downto 0);
signal a11_m31 :  bit_vector (2 downto 0);
signal a11_m41 :  bit_vector (3 downto 0);
signal a11_m51 :  bit_vector (4 downto 0);
signal a11_m9 :  bit_vector (8 downto 0);
signal a12_m31 :  bit_vector (2 downto 0);
signal a12_m41 :  bit_vector (3 downto 0);
signal a12_m51 :  bit_vector (4 downto 0);
signal a12_m9 :  bit_vector (8 downto 0);
signal a13_m31 :  bit_vector (2 downto 0);
signal a13_m41 :  bit_vector (3 downto 0);
signal a13_m51 :  bit_vector (4 downto 0);
signal a13_m9 :  bit_vector (8 downto 0);
signal a14_m31 :  bit_vector (2 downto 0);
signal a14_m41 :  bit_vector (3 downto 0);
signal a14_m51 :  bit_vector (4 downto 0);
signal a14_m9 :  bit_vector (8 downto 0);
signal a15_m31 :  bit_vector (2 downto 0);
signal a15_m41 :  bit_vector (3 downto 0);
signal a15_m51 :  bit_vector (4 downto 0);
signal a15_m9 :  bit_vector (8 downto 0);
signal a16_m31 :  bit_vector (2 downto 0);
signal a16_m41 :  bit_vector (3 downto 0);
signal a16_m51 :  bit_vector (4 downto 0);
signal a16_m9 :  bit_vector (8 downto 0);
signal a17_m31 :  bit_vector (2 downto 0);
signal a17_m41 :  bit_vector (3 downto 0);
signal a17_m51 :  bit_vector (4 downto 0);
signal a17_m9 :  bit_vector (8 downto 0);
signal a18_m31 :  bit_vector (2 downto 0);
signal a18_m41 :  bit_vector (3 downto 0);
signal a18_m51 :  bit_vector (4 downto 0);
signal a18_m9 :  bit_vector (8 downto 0);
signal a19_m31 :  bit_vector (2 downto 0);
signal a19_m41 :  bit_vector (3 downto 0);
signal a19_m51 :  bit_vector (4 downto 0);
signal a19_m9 :  bit_vector (8 downto 0);
signal a20_m31 :  bit_vector (2 downto 0);
signal a20_m41 :  bit_vector (3 downto 0);
signal a20_m51 :  bit_vector (4 downto 0);
signal a20_m9 :  bit_vector (8 downto 0);
signal a21_m31 :  bit_vector (2 downto 0);
signal a21_m41 :  bit_vector (3 downto 0);
signal a21_m51 :  bit_vector (4 downto 0);
signal a21_m9 :  bit_vector (8 downto 0);
signal a22_m31 :  bit_vector (2 downto 0);
signal a22_m41 :  bit_vector (3 downto 0);
signal a22_m51 :  bit_vector (4 downto 0);
signal a22_m9 :  bit_vector (8 downto 0);
signal a23_m31 :  bit_vector (2 downto 0);
signal a23_m41 :  bit_vector (3 downto 0);
signal a23_m51 :  bit_vector (4 downto 0);
signal a23_m9 :  bit_vector (8 downto 0);
signal a24_m31 :  bit_vector (2 downto 0);
signal a24_m41 :  bit_vector (3 downto 0);
signal a24_m51 :  bit_vector (4 downto 0);
signal a24_m9 :  bit_vector (8 downto 0);
signal a25_m31 :  bit_vector (2 downto 0);
signal a25_m41 :  bit_vector (3 downto 0);
signal a25_m51 :  bit_vector (4 downto 0);
signal a25_m9 :  bit_vector (8 downto 0);
signal a26_m31 :  bit_vector (2 downto 0);
signal a26_m41 :  bit_vector (3 downto 0);
signal a26_m51 :  bit_vector (4 downto 0);
signal a26_m9 :  bit_vector (8 downto 0);
signal a27_m31 :  bit_vector (2 downto 0);
signal a27_m41 :  bit_vector (3 downto 0);
signal a27_m51 :  bit_vector (4 downto 0);
signal a27_m9 :  bit_vector (8 downto 0);

signal b_m31 :  bit_vector (2 downto 0);
signal b_m41 :  bit_vector (3 downto 0);
signal b_m51 :  bit_vector (4 downto 0);
signal b_m9 :  bit_vector (8 downto 0);

signal rez : bit_vector (20 downto 0);-----------------------------------------------------------------

begin

bin_to_rns_1 : pss_to_rns PORT MAP(  x => r_a1, y1 => a1_m31, y2 => a1_m41, y3 => a1_m51, y4 => a1_m9);
bin_to_rns_2 : pss_to_rns PORT MAP(  x => r_a2, y1 => a2_m31, y2 => a2_m41, y3 => a2_m51, y4 => a2_m9);
bin_to_rns_3 : pss_to_rns PORT MAP(  x => r_a3, y1 => a3_m31, y2 => a3_m41, y3 => a3_m51, y4 => a3_m9);
bin_to_rns_4 : pss_to_rns PORT MAP(  x => r_a4, y1 => a4_m31, y2 => a4_m41, y3 => a4_m51, y4 => a4_m9);
bin_to_rns_5 : pss_to_rns PORT MAP(  x => r_a5, y1 => a5_m31, y2 => a5_m41, y3 => a5_m51, y4 => a5_m9);
bin_to_rns_6 : pss_to_rns PORT MAP(  x => r_a6, y1 => a6_m31, y2 => a6_m41, y3 => a6_m51, y4 => a6_m9);
bin_to_rns_7 : pss_to_rns PORT MAP(  x => r_a7, y1 => a7_m31, y2 => a7_m41, y3 => a7_m51, y4 => a7_m9);
bin_to_rns_8 : pss_to_rns PORT MAP(  x => r_a8, y1 => a8_m31, y2 => a8_m41, y3 => a8_m51, y4 => a8_m9);
bin_to_rns_9 : pss_to_rns PORT MAP(  x => r_a9, y1 => a9_m31, y2 => a9_m41, y3 => a9_m51, y4 => a9_m9);

bin_to_rns_10 : pss_to_rns PORT MAP(  x => g_a1, y1 => a10_m31, y2 => a10_m41, y3 => a10_m51, y4 => a10_m9);
bin_to_rns_11 : pss_to_rns PORT MAP(  x => g_a2, y1 => a11_m31, y2 => a11_m41, y3 => a11_m51, y4 => a11_m9);
bin_to_rns_12 : pss_to_rns PORT MAP(  x => g_a3, y1 => a12_m31, y2 => a12_m41, y3 => a12_m51, y4 => a12_m9);
bin_to_rns_13 : pss_to_rns PORT MAP(  x => g_a4, y1 => a13_m31, y2 => a13_m41, y3 => a13_m51, y4 => a13_m9);
bin_to_rns_14 : pss_to_rns PORT MAP(  x => g_a5, y1 => a14_m31, y2 => a14_m41, y3 => a14_m51, y4 => a14_m9);
bin_to_rns_15 : pss_to_rns PORT MAP(  x => g_a6, y1 => a15_m31, y2 => a15_m41, y3 => a15_m51, y4 => a15_m9);
bin_to_rns_16 : pss_to_rns PORT MAP(  x => g_a7, y1 => a16_m31, y2 => a16_m41, y3 => a16_m51, y4 => a16_m9);
bin_to_rns_17 : pss_to_rns PORT MAP(  x => g_a8, y1 => a17_m31, y2 => a17_m41, y3 => a17_m51, y4 => a17_m9);
bin_to_rns_18 : pss_to_rns PORT MAP(  x => g_a9, y1 => a18_m31, y2 => a18_m41, y3 => a18_m51, y4 => a18_m9);

bin_to_rns_19 : pss_to_rns PORT MAP(  x => b_a1, y1 => a19_m31, y2 => a19_m41, y3 => a19_m51, y4 => a19_m9);
bin_to_rns_20 : pss_to_rns PORT MAP(  x => b_a2, y1 => a20_m31, y2 => a20_m41, y3 => a20_m51, y4 => a20_m9);
bin_to_rns_21 : pss_to_rns PORT MAP(  x => b_a3, y1 => a21_m31, y2 => a21_m41, y3 => a21_m51, y4 => a21_m9);
bin_to_rns_22 : pss_to_rns PORT MAP(  x => b_a4, y1 => a22_m31, y2 => a22_m41, y3 => a22_m51, y4 => a22_m9);
bin_to_rns_23 : pss_to_rns PORT MAP(  x => b_a5, y1 => a23_m31, y2 => a23_m41, y3 => a23_m51, y4 => a23_m9);
bin_to_rns_24 : pss_to_rns PORT MAP(  x => b_a6, y1 => a24_m31, y2 => a24_m41, y3 => a24_m51, y4 => a24_m9);
bin_to_rns_25 : pss_to_rns PORT MAP(  x => b_a7, y1 => a25_m31, y2 => a25_m41, y3 => a25_m51, y4 => a25_m9);
bin_to_rns_26 : pss_to_rns PORT MAP(  x => b_a8, y1 => a26_m31, y2 => a26_m41, y3 => a26_m51, y4 => a26_m9);
bin_to_rns_27 : pss_to_rns PORT MAP(  x => b_a9, y1 => a27_m31, y2 => a27_m41, y3 => a27_m51, y4 => a27_m9);

f_2p4m1 : filter_mod_2p4m1 PORT MAP( x1 => a1_m41, x2 => a2_m41, x3 => a3_m41, x4 => a4_m41, x5 => a5_m41, 
														x6 => a6_m41, x7 => a7_m41, x8 => a8_m41, x9 => a9_m41, 
														x10 => a10_m41, x11 => a11_m41, x12 => a12_m41, x13 => a13_m41,
														x14 => a14_m41, x15 => a15_m41, x16 => a16_m41, x17 => a17_m41,
														x18 => a18_m41, x19 => a19_m41, x20 => a20_m41, x21 => a21_m41,
														x22 => a22_m41, x23 => a23_m41, x24 => a24_m41, x25 => a25_m41,
														x26 => a26_m41, x27 => a27_m41, y => b_m41);
f_2p3m1 : filter_mod_2p3m1 PORT MAP( x1 => a1_m31, x2 => a2_m31, x3 => a3_m31, x4 => a4_m31, x5 => a5_m31, 
														x6 => a6_m31, x7 => a7_m31, x8 => a8_m31, x9 => a9_m31, 
														x10 => a10_m31, x11 => a11_m31, x12 => a12_m31, x13 => a13_m31,
														x14 => a14_m31, x15 => a15_m31, x16 => a16_m31, x17 => a17_m31,
														x18 => a18_m31, x19 => a19_m31, x20 => a20_m31, x21 => a21_m31,
														x22 => a22_m31, x23 => a23_m31, x24 => a24_m31, x25 => a25_m31,
														x26 => a26_m31, x27 => a27_m31,y => b_m31);
f_2p5m1 : filter_mod_2p5m1 PORT MAP( x1 => a1_m51, x2 => a2_m51, x3 => a3_m51, x4 => a4_m51, x5 => a5_m51, 
														x6 => a6_m51, x7 => a7_m51, x8 => a8_m51, x9 => a9_m51,
														x10 => a10_m51, x11 => a11_m51, x12 => a12_m51, x13 => a13_m51,
														x14 => a14_m51, x15 => a15_m51, x16 => a16_m51, x17 => a17_m51,
														x18 => a18_m51, x19 => a19_m51, x20 => a20_m51, x21 => a21_m51,
														x22 => a22_m51, x23 => a23_m51, x24 => a24_m51, x25 => a25_m51,
														x26 => a26_m51, x27 => a27_m51, y => b_m51);
f2p9 : filter_mod_2p9 PORT MAP( x1 => a1_m9, x2 => a2_m9, x3 => a3_m9, x4 => a4_m9, x5 => a5_m9, 
												  x6 => a6_m9, x7 => a7_m9, x8 => a8_m9, x9 => a9_m9, 
												  x10 => a10_m9, x11 => a11_m9, x12 => a12_m9, x13 => a13_m9,
														x14 => a14_m9, x15 => a15_m9, x16 => a16_m9, x17 => a17_m9,
														x18 => a18_m9, x19 => a19_m9, x20 => a20_m9, x21 => a21_m9,
														x22 => a22_m9, x23 => a23_m9, x24 => a24_m9, x25 => a25_m9,
														x26 => a26_m9, x27 => a27_m9,y => b_m9);														

rns_to_bin : rns_to_pss PORT MAP ( y1 => b_m31, y2 => b_m41, y3 => b_m51, y4 => b_m9, z => rez);

f <= rez(20 downto 7);-------------------------------------------------

end Behavioral;

